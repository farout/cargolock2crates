Small tool to create CRATES entries for gentoo ebuilds from a Cargo.lock file.

Usage:
cargolock2crates or cargolock2crates -f &lt;filename&gt; &gt; &lt;output file&gt;

Then import the contents of the output file into the ebuild.
module gitlab.com/farout/cargolock2crates

go 1.21

require (
	github.com/pelletier/go-toml v1.9.5
	github.com/urfave/cli/v2 v2.27.1
	gitlab.com/bluebottle/go-modules/misc v0.0.7
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
)

# Go parameters
GOFMT      := gofmt
GOCMD      := go
GOBUILD    := GO111MODULE=on $(GOCMD) build -v -mod=vendor
GOCLEAN    := $(GOCMD) clean
GOINSTALL  := GO111MODULE=on $(GOCMD) install
GOTEST     := GO111MODULE=on $(GOCMD) test -v
GOGET      := GO111MODULE=on $(GOCMD) get -v
GOVET      := GO111MODULE=on $(GOCMD) vet
GORACE     := $(GOCMD) race
GOTOOL     := GO111MODULE=on $(GOCMD) tool

SRCS       := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")

export GO111MODULE=off

PACKAGES ?= $(shell find . -name "*.go" -print|grep -v /vendor/)

ifndef VERBOSE
.SILENT:
endif

.DEFAULT_GOAL := help

all: dep fmt test build coverage  ## Run all

dep: ## Get all the dependencies
		@echo "Getting dependencies"
		$(GOGET) -d ./...
		$(GOINSTALL) github.com/fzipp/gocyclo/cmd/gocyclo@latest
		$(GOINSTALL) github.com/uudashr/gocognit/cmd/gocognit@latest
		$(GOINSTALL) github.com/gordonklaus/ineffassign@latest
		$(GOINSTALL) github.com/securego/gosec/v2/cmd/gosec@latest
#		$(GOCMD) get -u github.com/client9/misspell/cmd/misspell
		$(GOINSTALL) github.com/go-critic/go-critic/cmd/gocritic@latest

fmt: ## Format source code
		@echo "Formatting code"
		GO111MODULE=on $(GOFMT) -w -s $(PACKAGES)

test: ## Running tests for files
		@echo "Running tests"
		@echo "============="
		#@echo "Running staticcheck"
		#GO111MODULE=on staticcheck -checks all
		@echo "Running gocyclo"
		-gocyclo -over 15 -ignore vendor .
		@echo "Running gocognit"
		-gocognit -over 15 .|grep -v vendor
		@echo "Running ineffassign"
		GO111MODULE=on  ineffassign .
		#@echo "Running misspell"
		#misspell . | grep -v vendor
		@echo "Running gosec"
		GO111MODULE=on gosec -no-fail -tests ./...
		@echo "Running gocritic"
		GO111MODULE=on gocritic check -enableAll -disable='commentedOutCode'
		@echo "Running $(GOVET)"
		$(GOVET) ./...
		@echo "Running $(GOTEST) race condition"
		$(GOTEST) -race ./...
		@echo "Running $(GOTEST) race memory sanity"
		CC=clang $(GOTEST) -msan ./.
		@echo "Running tests"
		$(GOTEST) ./. || exit 1;
		@echo "Running benchs"
		$(GOTEST) -bench ./.
		@echo ""

build: ## Building binary
		@echo "Running $(GOBUILD)"
		@echo "================"
		$(GOBUILD) -o cargolock2crates main.go;
		@echo ""

coverage: ## Generating coverage for files
		@echo "Running coverage"
		@echo "================"
		$(GOTEST) ./... -coverpkg=./... -coverprofile coverage.cov
		$(GOTOOL) cover -func=coverage.cov

clean: ./* ## Cleaning up
		@echo "Cleaning package"
		@echo "================"
		rm -f *.so
		rm -f *.cov
		rm -f cargolock2crates

help:  ## Displaying help for build targets
		@echo "Available targets in this makefile:"
		@echo ""
		@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

package main

import (
	"fmt"
	"os"

	"github.com/pelletier/go-toml"
	"github.com/urfave/cli/v2"

	"gitlab.com/bluebottle/go-modules/misc"
)

// LockFile is the definition for the lock file
type LockFile struct {
	Package []Definition `toml:"package"`
}

// Definition is the definition of each package entry
type Definition struct {
	Name         string   `toml:"name"`
	Version      string   `toml:"version"`
	Source       string   `toml:"source"`
	Checksum     string   `toml:"checksum"`
	Dependencies []string `toml:"dependencies,omitempty"`
}

// ReadCargoLock Reads Cargo.lock file and returns content.
func ReadCargoLock(filename string) (c LockFile, err error) {
	tfile, err := toml.LoadFile(filename)

	if err != nil {
		fmt.Println("Error when reading cargo file: " + err.Error())
	} else {
		err = tfile.Unmarshal(&c)
	} // if

	return c, err
} // ReadCargoLock ()

func mainWork(file string) (err error) {
	tmpContent, err := ReadCargoLock(file)

	if err == nil {
		fmt.Println("CRATES=\"")

		for _, v := range tmpContent.Package {
			fmt.Println(v.Name + "-" + v.Version)
		} // for

		fmt.Println("\"")
	} // if

	return
} // mainWork ()

func main() {
	var file string
	app := &cli.App{
		Name:                   "cargolock2crates",
		Usage:                  "Create CRATES entries for gentoo ebuilds from a Cargo.lock file (use -h for help)",
		UseShortOptionHandling: true,
		Version:                "0.1.2",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "file",
				Aliases:     []string{"f"},
				Usage:       "Name of the Cargo.lock file",
				Destination: &file,
				Value:       "Cargo.lock",
			},
		},
		Action: func(c *cli.Context) (err error) {
			if file != "" {
				err = mainWork(file)
			} else {
				fmt.Println(c.App.Usage)
			} // if

			return
		},
	}

	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrFatal")
} // main ()
